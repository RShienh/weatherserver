/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.google.gson.Gson;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author shienh
 */
@Path("weather")
@Produces(MediaType.APPLICATION_JSON)
public class WeatherService {

    public String appid = "7c68d8009ae4a9091932749c44648277";
    double kelvin = 273.15;

    @GET
    @Path("city/{q}")
    public String CityWeather(@PathParam("q") String q) throws JSONException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://api.openweathermap.org/data/2.5/weather?q=" + q + "&appid=" + appid);
        Invocation.Builder request = target.request();
        Response response = request.get();
        if (response.getStatus() == 200) {
            String output = response.readEntity(String.class);
            JSONObject jObject = new JSONObject(output);
            JSONArray a = jObject.getJSONArray("weather");
            int temp = (int) Math.round((double) jObject.getJSONObject("main").get("temp") - kelvin);
            int minTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_min") - kelvin);
            int maxTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_max") - kelvin);
            int humidity = (int) Math.round((int) jObject.getJSONObject("main").get("humidity"));
            int pressure = (int) Math.round((int) jObject.getJSONObject("main").get("pressure"));
            Weather weather = new Weather();
            weather.setCity(jObject.get("name").toString());
            weather.setCondition(a.getJSONObject(0).getString("main"));
            weather.setTemp(temp);
            weather.setMinTemp(minTemp);
            weather.setMaxTemp(maxTemp);
            weather.setHumidity(humidity);
            weather.setPressure(pressure);
            weather.setCountry(jObject.getJSONObject("sys").getString("country"));
            List<Weather> wList = Arrays.asList(weather);
            Gson gson = new Gson();
            return gson.toJson(wList);
        }
        return null;
    }

    private final ExecutorService executorService = java.util.concurrent.Executors.newCachedThreadPool();

    @GET
    @Path(value = "now")
    public void Weather(@Suspended final AsyncResponse asyncResponse) {
        executorService.submit(() -> {
            try {
                asyncResponse.resume(getWeather());
            } catch (JSONException ex) {
                Logger.getLogger(WeatherService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    @GET
    @Path(value = "latlng/{lat},{lng}")
    public String CoordinateWeather(@PathParam(value = "lat") String lat, @PathParam(value = "lng") String lng) throws JSONException {
        System.out.println(lat);
        System.out.println(lng);
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&appid=" + appid);
        Invocation.Builder request = target.request();
        Response response = request.get();
        if (response.getStatus() == 200) {
            String output = response.readEntity(String.class);
            JSONObject jObject = new JSONObject(output);
            JSONArray a = jObject.getJSONArray("weather");
            int temp = (int) Math.round((double) jObject.getJSONObject("main").get("temp") - kelvin);
            int minTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_min") - kelvin);
            int maxTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_max") - kelvin);
            int humidity = (int) jObject.getJSONObject("main").get("humidity");
            double pressure = (double) jObject.getJSONObject("main").get("pressure");
            Weather weather = new Weather();
            weather.setCity(jObject.get("name").toString());
            weather.setCondition(a.getJSONObject(0).getString("main"));
            weather.setTemp(temp);
            weather.setMinTemp(minTemp);
            weather.setMaxTemp(maxTemp);
            weather.setHumidity(humidity);
            weather.setPressure(pressure);
            weather.setCountry(jObject.getJSONObject("sys").getString("country"));
            List<Weather> wList = Arrays.asList(weather);
            Gson gson = new Gson();
            return gson.toJson(wList);
        }
        return null;
    }

    private String getWeather() throws JSONException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://api.openweathermap.org/data/2.5/weather?q=Toronto,CA&appid=" + appid);
        Invocation.Builder request = target.request();
        Response response = request.get();

        if (response.getStatus() == 200) {
            String output = response.readEntity(String.class);
            JSONObject jObject = new JSONObject(output);
            JSONArray a = jObject.getJSONArray("weather");
            int temp = (int) Math.round((double) jObject.getJSONObject("main").get("temp") - kelvin);
            int minTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_min") - kelvin);
            int maxTemp = (int) Math.round((double) jObject.getJSONObject("main").get("temp_max") - kelvin);
            int humidity = (int) Math.round((int) jObject.getJSONObject("main").get("humidity"));
            int pressure = (int) Math.round((int) jObject.getJSONObject("main").get("pressure"));
            Weather weather = new Weather();
            weather.setCity(jObject.get("name").toString());
            weather.setCondition(a.getJSONObject(0).getString("main"));
            weather.setTemp(temp);
            weather.setMinTemp(minTemp);
            weather.setMaxTemp(maxTemp);
            weather.setHumidity(humidity);
            weather.setPressure(pressure);
            weather.setCountry(jObject.getJSONObject("sys").getString("country"));
            List<Weather> wList = Arrays.asList(weather);
            Gson gson = new Gson();
            return gson.toJson(wList);
        }
        return null;
    }
}
